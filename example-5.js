import {cleanConsole, createAll} from './data';
import {getfield} from './example-4';
const companies = createAll();

cleanConsole(5, companies);
console.log('---- EXAMPLE 5 --- ', newfield(companies));

function newfield(data) {
  const fields = [];
  const field = [];
  for (let i = 0; i < data.length; i++) {
    const f = datausers(data[i]['users'], data[i]['usersLength']);
    field[i] = getfield(field[i], 'size', data[i]['usersLength']);
    field[i] = getfield(field[i], 'average', f[0]);
    field[i] = getfield(field[i], 'hasCar', f[1]);
    field[i] = getfield(field[i], 'averageWithCar', f[2]);
    fields.push(field[i]);
  }
  return fields;
}

function datausers(data, total) {
  let age = 0;
  let car = 0;
  let agecar = 0;
  let t = [];
  for (let i = 0; i < data.length; i++) {
    age = age + data[i]['age'];
    if (data[i]['car']) {
      car ++;
      agecar = agecar + data[i]['age'];
    }
  }
  age = age/total;
  agecar = agecar/car;
  t = [Math.round(age), car, Math.round(agecar)];
  return t;
}

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Use la función creada en el ejemplo 4 para crear una nueva función tomando
// como parámetro la variable "companies" y devuelve un nuevo objeto con los
// siguientes atributos:
//     'size' => total de "users"
//     'average' => edad promedio de "users"
//     'hasCar' => total de "users" propietarios de un carro
//     'averageWithCar' => edad promedio de los "users" con un carro

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Use the function created in example 4 to create a
// new function taking as parameter the "companies" variable and returning
// a new object with the following attributes:
//     'size' => number of "users"
//     'average' => average age of "users"
//     'hasCar' => number of "users" owning a car
//     'averageWithCar' => average age of users with a car

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Utiliser la fonction créée dans l'exemple 4 pour créer une
// nouvelle fonction prenant en paramètre la variable "companies" et renvoyant
// un nouvel objet avec les attributs suivants :
//     'size' => nombre de "users"
//     'average' => moyenne d'âge des "users"
//     'hasCar' => nombre de "users" possédant une voiture
//     'averageWithCar' => moyenne d'âge des "users" possédant une voiture
