import {createAll, cleanConsole} from './data';
const companies = createAll();

cleanConsole(1, companies);
console.log('---- EXAMPLE 1 --- ', letters(companies));

function letters(data) {
  if (data.length!=0) {
    for (let i = 0; i < data.length; i++) {
      data[i]['name'] = letter(data[i]['name']);
      data[i]['users'] = users(data[i]['users']);
    }
  }
  return data;
}

// replace undefined with empty and sort array
function users(data) {
  for (let i = 0; i < data.length; i++) {
    if (data[i]['firstName'] === undefined) {
      data[i]['firstName'] = '';
    } else if (data[i]['lastName'] === undefined) {
      data[i]['lastName'] = '';
    } else {
      data[i]['firstName'] = letter(data[i]['firstName']);
      data[i]['lastName'] = letter(data[i]['lastName']);
    }
  }
  return data.sort();
}

function letter(data) {
  return data.replace(/\b\w/g, l => l.toUpperCase());
}

export function getletters(data) {
  return letters(data);
}

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Crear una función tomando la variable "companies" como parámetro y reemplazando
// todos los valores "undefined" en "usuarios" por un string vacío.
// El nombre de cada "company" debe tener una letra mayúscula al principio, así como
// el apellido y el nombre de cada "user".
// Las "companies" deben ordenarse por su total de "user" (orden decreciente)
// y los "users" de cada "company" deben aparecer en orden alfabético.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the variable "companies" as a parameter and replacing
// all values "undefined" in "users" by an empty string.
// The name of each "company" must have a capital letter at the beginning as well as
// the last name and first name of each "user".
// The "companies" must be sorted by their number of "user" (decreasing order)
// and the "users" of each "company" must be listed in alphabetical order.

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Créer une fonction prenant en paramètre la variable "companies" et remplaçant
// toutes les valeurs "undefined" dans les "users" par un string vide.
// Le nom de chaque "company" doit avoir une majuscule au début ainsi que
// le nom et le prénom de chaque "user".
// Les "companies" doivent être triées par leur nombre de "user" (ordre décroissant)
// et les "users" de chaque "company" doivent être classés par ordre alphabétique.
